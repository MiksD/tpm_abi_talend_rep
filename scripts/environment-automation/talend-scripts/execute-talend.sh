#!/bin/bash

if [ $# -eq 0 ]
	then
		echo "No arguments supplied"
elif [ $# -eq 15 ]
	then
		echo "Parameters Supplied"

		#Salesforce Credentials
		sf_username="${1}"
		sf_password="${2}"
        sf_securitytoken="${3}"
		sf_domain="${4}"
		sf_server="${5}"
		talend_job="${6}"
		talend_job_lower=`echo "${talend_job,,}"`
		talend_job_version="${7}"
		artifact="${talend_job}_${talend_job_version}.zip"
		WORKSPACE="${8}"
		talend_import_export="${9}"
		git_commit_usage="${10}"
		git_url="${11}"
		release_data_folder="${12}"
		filepath_MASTER="${13}"
		filepath_ENV1="${14}"
		filepath_ENV2="${15}"

		echo "sf_username": ${sf_username}
		echo "sf_password": ${sf_password}
        echo "sf_securitytoken": ${sf_securitytoken}
		echo "sf_domain": ${sf_domain}
		echo "sf_server": ${sf_server}
		echo "talend_job": ${talend_job}
		echo "talend_job_lower": ${talend_job_lower}
		echo "talend_job_version": ${talend_job_version}
		echo "artifact": ${artifact}
		echo "WORKSPACE": ${WORKSPACE}
		echo "talend_import_export": ${talend_import_export}
		echo "git_commit_usage": ${git_commit_usage}
		echo "git_url": ${git_url}
		echo "release_data_folder": ${release_data_folder}
		echo "filepath_MASTER": ${filepath_MASTER}
		echo "filepath_ENV1": ${filepath_ENV1}
		echo "filepath_ENV2": ${filepath_ENV2}

else
	echo "ERROR > Incorrect Parameters Supplied"

	echo "sf_username": ${1}
	echo "sf_password": ${2}
    echo "sf_securitytoken": ${3}
	echo "sf_domain": ${4}
	echo "sf_server": ${5}
	echo "talend_job": ${6}
	echo "talend_job_version": ${7}
	echo "WORKSPACE": ${8}
	echo "talend_import_export": ${9}
	echo "git_commit_usage": ${10}
	echo "git_url": ${11}
	echo "release_data_folder": ${12}
	echo "filepath_MASTER": ${13}
	echo "filepath_ENV1": ${14}
	echo "filepath_ENV2": ${15}


	exit 1
fi

#Invalid password characters not supported by Talend
if [ `echo "${sf_password}" | grep "&" | wc -l` -gt 0 ]; then
	echo "ERROR"
	echo "ERROR: Password uses an invalid character not supported by Talend"
	echo "ERROR"
	exit 1
fi


stashWORKSPACE=`pwd`
cd scripts
WORKSPACE=`pwd`
echo "Workspace $WORKSPACE"

sf_api_version="/services/Soap/u/39.0"
sf_api_version="/services/Soap/u/39.0"
endpoint="${sf_domain}${sf_api_version}"

#talend_job="Main_General"
talend_project="tpm_abi"
export_location="${WORKSPACE}/scripts/environment-automation/exports/"
configuration_file_location="${WORKSPACE}/scripts/environment-automation/talend-build/build/"
checkin_export_location="${stashWORKSPACE}/data/tpm_abi_talend_rep/data/${release_data_folder}/"
backup_location="${stashWORKSPACE}/data/tpm_abi_talend_rep/data/backup/"
local_export_location="${export_location}data_export/Business_Templates"

selected_templates=`head -1 ../selected_templates.cfg`
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "INFO > Talend Job list."
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo " Selected Templates: ${selected_templates}"

echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "INFO > Talend Job list."
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

old_IFS=$IFS
export IFS=","

for template in $sf_template
do
	selected_templates+=($template)
done
IFS=${old_IFS}

if [ ${#selected_templates[@]} -eq 0 ]; then
	echo "ERROR > Selected Templates = 0"
	exit 1
else
	echo "INFO > Templates selected."

	mkdir -p ${configuration_file_location}
	echo "SFObject;Skip;Condition" > "${configuration_file_location}/configuration.csv"

	for template in ${selected_templates[@]}
	do
		echo "INFO > JOB: $template"
		echo "${template};" >> "${configuration_file_location}/configuration.csv"
	done

	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
	cat ${configuration_file_location}/configuration.csv
	echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

fi

echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "INFO > Talend Job list."
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

generate_build_file(){
case $1 in
	"general")
		  cat > "${WORKSPACE}/scripts/environment-automation/talend-build/$talend_job/build.xml" << EOF

		  <project>
		  <property name="lib" location="../lib/*" />
		  <property name="job" location="${talend_job_lower}_0_1.jar" />


		  <target name="general">
				  <java classname="${talend_project}.${talend_job_lower}_0_1.${talend_job}" classpath="\${lib}:\${job}" fork="true" maxmemory="1024m">
					<arg line="--context_param sf_username=${sf_username}" />
					<arg line="--context_param sf_password=${sf_password}" />
					<arg line="--context_param sf_securitytoken=${sf_securitytoken}" />
					<arg line="--context_param export_file_location=${export_location}" />
					<arg line="--context_param sf_domain=${sf_domain}" />
					<arg line="--context_param sf_api_version=${sf_api_version}" />
					<arg line="--context_param endpoint=${endpoint}" />
					<arg line="--context_param configuration_file_location=${configuration_file_location}"/>
					<arg line="--context_param aws_access_key=${aws_access_key}" />
					<arg line="--context_param aws_secret_key=${aws_secret_key}" />
					<arg line="--context_param aws_usage=${aws_usage}" />
					<arg line="--context_param filepath_MASTER=${filepath_MASTER}" />
					<arg line="--context_param filepath_ENV1=${filepath_ENV1}" />
					<arg line="--context_param filepath_ENV2=${filepath_ENV2}" />
				  </java>
		  </target>
		  </project>
EOF
	;;
esac
}

echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"
echo "INFO > Talend Template export."
echo "INFO > Running Job for Selected Templates."
#echo "INFO > Talend Org Context Property file = $talend_context_prop"
echo "INFO > Processing latest JOB artifact = $artifact"
echo "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"

cd ${WORKSPACE}/scripts/environment-automation/talend-build

if [ $? -eq 0 ] && [ -f $artifact ]; then

	if [ -f $artifact ]; then
		unzip -q $artifact
		#rm -f $artifact

		cd ../../..
		generate_build_file "general"

		# generate the configuration.csv file in the build dir,so that the job class picks up only the selected templates
		touch "log.txt"

		echo "INFO > Running Job."

		#################################################################
		#### Copy files from cloned branch to local workspace
		#################################################################
		if [ ${talend_import_export} == "IMPORT" ]; then
			if [ ${git_commit_usage} == "true" ]; then
				echo "################################"
				cd ${WORKSPACE}
				mkdir -p scripts/environment-automation/exports/data_export/Business_Templates
				cp ${checkin_export_location}*.csv ${export_location}
				echo "Files on local filesystem to be used by Talend Jobs:"
				ls -ltr ${export_location}
				echo "################################"
			else
				echo "No commits made, GIT Commit: ${git_commit_usage}"
				echo "################################"
			fi
		else
			echo "No commits made, GIT Commit: ${git_commit_usage}"
			echo "################################"
		fi

		#################################################################
		#### Execute Ant
		#### Pipe output to a file
		#### Search the output file
		#################################################################
		cd ${WORKSPACE}/scripts/environment-automation/talend-build/$talend_job/
		ant -version
		ant general 2>&1 | tee ant_output_log.log

		#################################################################
		#### Copy files into cloned branch
		#### Push changes to stash
		#################################################################
		echo "################################"
		if [ ${talend_import_export} == "EXPORT" ]; then
			echo "Export jobs commit changes to stash"
			if [ `ls ${export_location}*.csv | wc -l` -ne 0 ]; then
				for i in `ls ${export_location}*.csv`
				do
					echo "Move file to Stash: ${i}"
					cp ${i} ${checkin_export_location}
				done

				if [ ${git_commit_usage} == "true" ]; then
					echo "Attempting commit, GIT Commit: ${git_commit_usage}"
					cd ${checkin_export_location}
					git status

					for i in `ls *.csv`
					do
						git add ${i}
					done
					git commit -m "Jenkins Export from ${sf_domain}"
					git push ${git_url}

				else
					echo "No commits made, GIT Commit: ${git_commit_usage}"
				fi
			fi
		else
			echo "Import jobs no changes to commit to stash"
		fi

		#################################################################
		#### Error reporting of Talend job
		#################################################################
		echo "################################"
		cd ${WORKSPACE}/scripts/environment-automation/talend-build/$talend_job/
		if [ `grep "ERROR" ant_output_log.log | wc -l` -eq 0 ]; then
			echo "INFO > Talend Job Success"
			currentBuild.result = "Success"
			return currentBuild.result
		else
			echo "ERROR > Talend Job ERROR"
			exit 1
		fi
		echo "" > ant_output_log.log

	else
		echo "ERROR > Artifact $artifact does not exist."
		exit 1
	fi
else
	echo "ERROR > Artifact $artifact does not exist."
	exit 1
fi
